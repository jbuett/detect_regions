FROM tensorflow/tensorflow:1.14.0
#FROM dpokidov/imagemagick
FROM python:3.6.8


#RUN apt-get install libmagickwand-dev
RUN apt-get update
RUN apt-get -y  install ghostscript
RUN apt-get install imagemagick

RUN mkdir -p /app
WORKDIR /app

ADD . ./

#ADD detect_illu_web.py .
#ADD images_paths.npy .
#ADD images_bw.np .


RUN pip install --no-cache-dir --upgrade pip
RUN pip install -r requirements.txt

# RUN pip install --upgrade pip setuptools wheel
# RUN pip install keras==2.2.5
# RUN pip install https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-1.14.0-py3-none-any.whl
# RUN pip install cherrypy
# RUN pip install Pillow
# RUN pip install opencv-python
# RUN pip install numpy
# RUN pip install imagehash
# RUN pip install scikit-image
# RUN pip install coverage
# RUN pip install wand



EXPOSE 80


CMD [ "python", "./detect_illu_web.py" ]