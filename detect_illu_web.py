import os
import io
import os.path
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from io import BytesIO
from wand.image import Image
from PIL import Image as myImage
import numpy as np
from yolo import YOLO




import cherrypy
from cherrypy.lib import static
cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 80,
                       })

yolo = YOLO()


localDir = os.path.dirname(__file__)
absDir = os.path.join(os.getcwd(), localDir)


class detect_illu_web(object):

    @cherrypy.expose
    def index(self):
        return """
        <html><body>
            <h2>Extract content illustrations from the PDF of an early modern book</h2>
            <form action="upload" method="post" enctype="multipart/form-data">
              Please submit a suitable PDF:<br>
              <input type="file" name="myFile">
              <br /><button>Submit</button>
            </form>
            <form action="upload_default" method="post">
              If you do not have a suitable PDF at hand, use the one we provide here as default:<br>
              <br /><button>Use default</button>
            </form>
            <p>N.B: Due to limited resources on this virtual machine, the next step is painstakingly slow. I suggest 
            you press submit and go have a coffee.</p>
        </body></html>
        """

    @cherrypy.expose
    def upload_default(tmp):
        #try:
        global full_data
        global size
        global data
        global myFileName
        global myContent_type
        size = 0
        myFileName = "sphaera.pdf"
        myContent_type ='PDF'
        full_data  = bytearray()
        with open("sphaera.pdf", "rb") as myFile:
              full_data = myFile.read()

              size += len(full_data)

        print(full_data[:50], size)
        raise cherrypy.InternalRedirect('/uploaded')


    @cherrypy.expose
    def upload(self, myFile=None):
        global full_data
        global size
        global data
        global myFileName
        global myContent_type
        try:
          size = 0
          global full_data
          full_data  = bytearray()
          while True:
            data = myFile.file.read(8192)
            if not data:
                break
            full_data += data #.decode('utf-8')   
            size += len(data)
        except:
          out = """<html>
          <body>
          Cannot read submitted file or file is empty
          </body>
          </html>"""
          return out
        myFileName, myContent_type = myFile.filename, myFile.content_type
        raise cherrypy.InternalRedirect('/uploaded')

    @cherrypy.expose
    def uploaded(self):
        print(size)
        out = """<html>
        <body>
            File length: %s<br />
            File filename: %s<br />
            File mime-type: %s<br />
            File begins with: %s ...<br />
           <p>
           Your upload seems ok. Do you want to proceed?
            <form method="post" action="process_new">
              <button type="submit">Ok, go ahead</button>
            </form>
           </p>
           <p>
           This step, if anything, is even slower. What about another coffee?
           </p>
        </body>
        </html>"""
        return out % (size, myFileName, myContent_type, full_data[:50])


    @cherrypy.expose
    def breaking(self):
       return """<html>
          <head></head>
          <link rel="import" href="process_new">
          </body>
        </html>"""

    @cherrypy.expose
    def process_new(self):

        global imgs
        imgs=[]
        
        with(Image(blob=io.BytesIO(full_data), format='pdf', resolution=100)) as source: 

                images = source.sequence
                images.format = 'gray'
                images.alpha_channel = False
                pages = len(images)
                out=""
                for i in range(pages):
                   img_buffer = np.asarray(bytearray(images[i].clone().make_blob('jpeg')), dtype='uint8')
                   bytesio = BytesIO(img_buffer)
                   imgs.append(bytesio)
                   out +="<img  src=\"single_page?nr="+str(i)+"\" width=\"200\">"
                   #detected=yolo.detect_image(image)
                   #boxed, nr_boxes=np.array(detected[0]), detected[1]
                   #if not nr_boxes==False:
                      #pages_illu.append(boxed)
        return """<html>
          <head></head>
          <body>
          <p>
           It worked! Here is your PDF broken up into single pages:<br />
           I am now ready to find your content illustrations.
           I suggest lunch this time.
            <form method="post" action="find_illu">
              <button type="submit">Ok, go ahead</button>
            </form>
           </p>"""+out+"""
          </div>
          </body>
        </html>"""

        
    @cherrypy.expose
    def single_page(self, nr=None, boxed=False):
        nr=int(nr)
        cherrypy.response.headers['Content-Type'] = "image/png"
        if boxed:
           page=pages_illu[nr]
           page = myImage.fromarray(page)
           imgByteArr = io.BytesIO()
           page.save(imgByteArr, format='png')
           contents = imgByteArr.getvalue()
        else:
           page=imgs[nr]
           contents = page.getvalue()
        return contents
          	
    def tmp(self):
        cherrypy.response.headers['Content-Type'] = "image/png"

        row_nr=len(pages_illu)//2
        if len(pages_illu)%2 != 0:
            row_nr+=1
            plot_nr=1
            fig=plt.figure(figsize=(6, row_nr*3))
            for img in pages_illu:
                plt.subplots_adjust(hspace = .05, wspace = .05, left=.05)
                plt.subplot(row_nr, 2, plot_nr)
                plt.imshow(img)
                plot_nr +=1
        figdata=BytesIO()
        #fig=savefig()
        fig.savefig(figdata, format='png')
        fig.savefig('tmp.png')
        imdata = figdata.getvalue()
        return imdata#plt.show()


        #f = open("tut01.py", "r")
        #cherrypy.log(f.read())
        #return out % (size, myFile.filename, myFile.content_type, full_data[:50])

    @cherrypy.expose
    def find_illu(self):
        global pages_illu
        pages_illu=[]
        try:
            out=""
            i=0
            for image in imgs:
                image = myImage.open(image)
                detected=yolo.detect_image(image)
                boxed, nr_boxes=np.array(detected[0]), detected[1]
                #if not nr_boxes==False:
                pages_illu.append(boxed)
                out +="<img  src=\"single_page?nr="+str(i)+"&boxed=True\" width=\"200\">"
                i+=1

        except:
            raise
        row_nr=len(pages_illu)//2
        if len(pages_illu) != 0:
            return """<html>
            <head>Here are your illustrations:<br /></head>
            <body>
            <div style="background-color:#efffcc">"""+out+"""
            </div>
            </body>
           </html>"""
        else:
           return "did not find any illustrations"
    @cherrypy.expose
    def images(self):
        cherrypy.response.headers['Content-Type'] = "image/png"
        fig = plt.figure()
        plt.plot([1,2,3,4])

        figdata=BytesIO()
        fig.savefig(figdata, format='png')

        imdata = figdata.getvalue()
        return imdata
    


tutconf = os.path.join(os.path.dirname(__file__), 'tutorial.conf')


cherrypy.quickstart(detect_illu_web())