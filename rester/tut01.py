import cherrypy

import random
import string


class HelloWorld(object):
    @cherrypy.expose
    def index(self):
        return "Hello world!"
    
    @cherrypy.expose
    def generate(self):
        return ''.join(random.sample(string.hexdigits, 8))
        
    @cherrypy.expose
    def strauss(self):
        return ";-)"

cherrypy.quickstart(HelloWorld())