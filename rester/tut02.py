import random
import string

import cherrypy


class StringGenerator(object):
    @cherrypy.expose
    def index(self):
        return """<html>
          <head></head>
          <body>
			<form method="post" enctype="multipart/form-data" action="generate">
 				 <label>please choose a pdf.
   					 <input type="file" size="50" name="datei" > 
 				 </label>  
 			<button>… Upload</button>
			</form>
          </body>
        </html>"""

    @cherrypy.expose
    def generate(self, datei):
        print(datei)
        return ''

cherrypy.quickstart(StringGenerator())